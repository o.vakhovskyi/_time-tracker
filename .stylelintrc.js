module.exports = {
  extends: ['stylelint-config-sass-guidelines', 'stylelint-config-prettier'],
  rules: {
    'selector-type-no-unknown': null,
    'max-nesting-depth': 10,
    'selector-no-qualifying-type': null,
    'selector-max-compound-selectors': 10,
    'selector-pseudo-class-allowed-list': [
      'ng-deep',
      'host',
      'hover',
      'not',
      'focus',
      'first-child',
      'last-child',
      'nth-child',
      'active',
    ],
    'selector-pseudo-element-no-unknown': [
      true,
      {
        ignorePseudoElements: ['ng-deep'],
      },
    ],
  },
};
