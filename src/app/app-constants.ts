import { IActionButtonData, IInputData, IOptionData } from '../interfaces/components-data';

export const PATTERN_FOR_TIME = '([01]?[0-9]|2[0-3])([.])[0-5][0-9]';

export const WEEK_BTN_DATA: IActionButtonData = {
  color: 'primary',
  disabled: false,
  placeholder: 'DAY',
  autoFocus: false,
};

export const DAY_BTN_DATA: IActionButtonData = {
  color: 'primary',
  disabled: false,
  placeholder: 'WEEK',
  autoFocus: false,
};

export const SEND_BTN_DATA: IActionButtonData = {
  color: 'warn',
  disabled: false,
  placeholder: 'Send',
  autoFocus: false,
  type: 'submit',
};

export const INPUT_TYPE_NUMBER_DATA: IInputData = {
  type: 'text',

  placeholder: '',
  required: false,

  label: 'Write down your time',
  visibilityValue: true,
};

export const TEXT_AREA_DATA: IInputData = {
  type: 'text',

  placeholder: '',
  required: false,
  label: 'Notes',
};

export const OPTIONS_FOR_SELECT_PROJECTS: IOptionData[] = [
  {
    value: 'Option #1',
    title: 'Option #1',
  },
  {
    value: 'Option #2',
    title: 'Option #2',
  },
  {
    value: 'Option #3',
    title: 'Option #3',
  },
  {
    value: 'Option #4',
    title: 'Option #4',
  },
];

export const OPTIONS_FOR_SELECT_TASKS: IOptionData[] = [
  {
    value: 'Task #1',
    title: 'Task #1',
  },
  {
    value: 'Task #2',
    title: 'Task #2',
  },
  {
    value: 'Task #3',
    title: 'Task #3',
  },
  {
    value: 'Task #4',
    title: 'Task #4',
  },
];

export const CACHE_FORM_DATA = {
  selected_project: [
    {
      value: 'Option #1',
      title: 'Option #1',
    },
    {
      value: 'Option #2',
      title: 'Option #2',
    },
    {
      value: 'Option #3',
      title: 'Option #3',
      selected: true,
    },
    {
      value: 'Option #4',
      title: 'Option #4',
    },
  ],
  selected_task: [
    {
      value: 'Task #1',
      title: 'Task #1',
    },
    {
      value: 'Task #2',
      title: 'Task #2',
      selected: true,
    },
    {
      value: 'Task #3',
      title: 'Task #3',
    },
    {
      value: 'Task #4',
      title: 'Task #4',
    },
  ],
  input_time: {
    type: 'text',
    value: 4.55,
    placeholder: '',
    required: false,
    label: 'Write down your time',
    visibilityValue: true,
  },

  job_notes: {
    type: 'text',
    value: 'Cache value',
    placeholder: '',
    required: false,
    label: 'Notes',
  },
};

export const AUTH_DATA = {
  loginData: {
    type: 'text',
    placeholder: '',
    required: false,
    label: 'Login',
  },
  passwordData: {
    type: 'text',
    placeholder: '',
    required: false,
    label: 'Password',
    icon: 'visibility_off',
  },
};
export const PASSWORD_VISIBILITY = {
  visibilityON: 'visibility_on',
  visibilityOFF: 'visibility_off',
};
