import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AuthGuards } from 'src/app/auth-guards';
import { LoginedGuards } from 'src/app/logined-guard';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AuthUserService } from './repository/auth-user.service';

@NgModule({
  declarations: [AppComponent, HeaderComponent, FooterComponent],
  imports: [BrowserModule, CommonModule, BrowserAnimationsModule, AppRoutingModule, MatToolbarModule, HttpClientModule],
  providers: [AuthGuards, AuthUserService, LoginedGuards],
  bootstrap: [AppComponent],
})
export class AppModule {}
