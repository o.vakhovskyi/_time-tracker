import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import { UserStateService } from './repository/user-state.service';

@Injectable()
export class AuthGuards implements CanActivate {
  sub!: Subscription;
  constructor(private userState: UserStateService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    let res = false;
    this.sub = this.userState.isAuthenticated().subscribe((x) => {
      if (!!x) {
        return (res = true);
      } else {
        void this.router.navigateByUrl('/login');

        return (res = false);
      }
    });

    return res;
  }
}
