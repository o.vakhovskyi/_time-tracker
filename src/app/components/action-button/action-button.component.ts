import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'ttip-action-button',
  templateUrl: './action-button.component.html',
  styleUrls: ['./action-button.component.scss'],
})
export class ActionButtonComponent {
  @Input() color: ThemePalette;
  @Input() disabled?: boolean;
  @Input() type?: string;
  @Input() placeholder?: string;
  @Input() icon?: string;
  @Input() autofocus?: boolean;

  @Output() clicked: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

  buttonClicked(event: MouseEvent): void {
    this.clicked.emit(event);
  }
}
