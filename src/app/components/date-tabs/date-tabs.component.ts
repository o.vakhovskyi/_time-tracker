import { Component, Input, EventEmitter, Output } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { IDateTabData } from '../../../interfaces/components-data';
import { CustomWeekDatePipe } from './week-date.pipe';

@Component({
  selector: 'ttip-date-tabs',
  templateUrl: './date-tabs.component.html',
  styleUrls: ['./date-tabs.component.scss'],
  providers: [CustomWeekDatePipe],
})
export class DateTabsComponent {
  @Input() data!: IDateTabData[];
  @Input() selectedIndex?: number;
  @Input() viewType!: string;

  @Output() selectedChange: EventEmitter<number> = new EventEmitter<number>();

  onChanged(event: MatTabChangeEvent): void {
    this.selectedChange.emit(event.index);
  }
}
