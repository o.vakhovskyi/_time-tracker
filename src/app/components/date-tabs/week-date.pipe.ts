import { Pipe, PipeTransform } from '@angular/core';

import * as dayjs from 'dayjs';
import * as weekOfYear from 'dayjs/plugin/weekOfYear';
import * as weekday from 'dayjs/plugin/weekday';

export const WEEKDAY = 6;

dayjs.extend(weekOfYear);
dayjs.extend(weekday);
@Pipe({
  name: 'customWeekDate',
})
export class CustomWeekDatePipe implements PipeTransform {
  transform(value: number): string {
    const str = `${dayjs(value).weekday(0).format('D/M/dd')} - ${dayjs(value).weekday(WEEKDAY).format('D/M/dd')}`;

    return str;
  }
}
