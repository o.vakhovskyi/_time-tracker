import { Component, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthUserService } from 'src/app/repository/auth-user.service';
import { UserStateService } from 'src/app/repository/user-state.service';
import { Loger } from 'src/app/utils/decorator-helper';

@Component({
  selector: 'ttip-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnChanges {
  isAuthenticated!: Observable<boolean>;

  constructor(private authService: AuthUserService, private userState: UserStateService, private router: Router) {}

  ngOnInit(): void {
    this.isAuthenticated = this.userState.isAuth;
  }
  ngOnChanges() {}

  logAction() {
    if (localStorage.getItem('token')) {
      this.authService.logOut();
    } else {
      void this.router.navigateByUrl('/login');
    }
  }
}
