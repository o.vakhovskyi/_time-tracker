import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ttip-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputTextComponent,
      multi: true,
    },
  ],
})
export class InputTextComponent implements ControlValueAccessor {
  @Input() type = 'text';
  @Input() placeholder = '';
  @Input() visibilityValue?: boolean = false;
  @Input() label?: string;
  @Input() value?: string;
  @Input() icon?: string;
  @Output() changeIcon: EventEmitter<void> = new EventEmitter<void>();

  // Function to call when the input is touched.
  onChange = (value: string | undefined) => {};
  // Function to call when the value changes.
  onTouched = (value: string | undefined) => {};

  inputChanged(event: Event): void {
    const input = event.target as HTMLInputElement;
    this.writeValue(input?.value);
  }

  writeValue(value: string | undefined): void {
    this.value = value;
    this.onChange(value);
    this.onTouched(value);
  }

  registerOnChange(fn: (value: string | undefined) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  iconEvent() {
    this.changeIcon.emit();
  }
}
