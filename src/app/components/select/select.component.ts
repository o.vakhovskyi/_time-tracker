import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ttip-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: SelectComponent,
      multi: true,
    },
  ],
})
export class SelectComponent implements ControlValueAccessor {
  @Input() options!: { value: string; title: string }[];
  value!: string | undefined;

  onChange = (value: string | undefined) => {};
  onTouched = (value: string | undefined) => {};

  optionSelected(selectedOption: string): void {
    this.writeValue(selectedOption);
    this.onChange(selectedOption);
  }

  writeValue(value: string | undefined): void {
    this.value = value;

    // this.onChange(value);

    this.onTouched(value);
  }

  registerOnChange(fn: (value: string | undefined) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
}
