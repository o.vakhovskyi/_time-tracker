import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ttip-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: TextareaComponent,
      multi: true,
    },
  ],
})
export class TextareaComponent implements ControlValueAccessor {
  @Input() value?: string | undefined;
  @Input() label?: string;

  onChange = (value: string | undefined) => {};
  onTouched = (value: string | undefined) => {};
  inputChanged(event: Event): void {
    const target = event.target as HTMLTextAreaElement;
    this.writeValue(target.value);
  }

  writeValue(value: string | undefined): void {
    this.value = value;
    this.onChange(value);
    this.onTouched(value);
  }

  registerOnChange(fn: (value: string | undefined) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
}
