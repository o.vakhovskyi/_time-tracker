import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// MATERIAL MODULES
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';

// COMPONENTS
import { ActionButtonComponent } from './action-button/action-button.component';
import { DateTabsComponent } from './date-tabs/date-tabs.component';
import { TextareaComponent } from './textarea/textarea.component';
import { InputTextComponent } from './input-text/input-text.component';
import { SelectComponent } from './select/select.component';
import { ProjectListComponent } from '../modules/time-tracking/components/project-list/project-list.component';
import { TimeCardComponent } from '../modules/time-tracking/components/time-card/time-card.component';

// PIPES
import { CustomWeekDatePipe } from './date-tabs/week-date.pipe';

const components = [
  DateTabsComponent,
  ActionButtonComponent,
  TextareaComponent,
  InputTextComponent,
  SelectComponent,
  ProjectListComponent,
  TimeCardComponent,
];

const materialModules = [
  MatTabsModule,
  MatButtonModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCardModule,
];

@NgModule({
  imports: [CommonModule, FormsModule, ...materialModules],
  declarations: [...components, CustomWeekDatePipe],
  exports: [...components, FormsModule],
})
export class UiModule {}
