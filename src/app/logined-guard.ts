import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { UserStateService } from './repository/user-state.service';

@Injectable()
export class LoginedGuards implements CanActivate {
  sub!: Subscription;
  constructor(private userState: UserStateService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    let res = false;
    this.sub = this.userState.isAuthenticated().subscribe((x) => {
      if (!!x) {
        void this.router.navigateByUrl('/tracker');

        return (res = false);
      } else {
        return (res = true);
      }
    });

    return res;
  }
}
