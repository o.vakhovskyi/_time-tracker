import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UiModule } from 'src/app/components/ui.module';
import { LoginComponent } from './pages/login/login.component';
import { AuthFormComponent } from './components/auth-form/auth-form.component';
import { AuthUserService } from '../../repository/auth-user.service';

import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [LoginComponent, AuthFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UiModule,
    RouterModule.forChild([{ path: '', component: LoginComponent }]),
    MatCardModule,
  ],
  exports: [RouterModule],
  providers: [],
})
export class AuthModule {}
