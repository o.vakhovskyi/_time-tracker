import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PASSWORD_VISIBILITY } from 'src/app/app-constants';

import { IActionButtonData, IAuthData, IAuthFormValue } from 'src/interfaces/components-data';
@Component({
  selector: 'ttip-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.scss'],
})
export class AuthFormComponent implements OnInit {
  form!: FormGroup;

  passIcon = PASSWORD_VISIBILITY;
  iconFlag = true;
  @Input() formData!: IAuthData;
  @Input() button!: IActionButtonData;
  @Output() usersInfo: EventEmitter<IAuthFormValue> = new EventEmitter<IAuthFormValue>();
  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      login: new FormControl('user', [Validators.minLength(3), Validators.required]),
      password: new FormControl('', [Validators.minLength(3), Validators.required]),
    });
  }

  submitForm() {
    if (this.form.valid) {
      this.usersInfo.emit(this.form.value);

      this.form.reset();
      this.form.markAsUntouched();
    }
  }

  passwordView() {
    this.iconFlag = !this.iconFlag;
    this.formData.passwordData.icon = this.iconFlag ? this.passIcon.visibilityOFF : this.passIcon.visibilityON;
    this.formData.passwordData.type = this.iconFlag ? 'text' : 'password';
  }
}
