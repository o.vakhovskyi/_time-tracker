import { Component, OnDestroy, OnInit } from '@angular/core';
import { SEND_BTN_DATA, AUTH_DATA } from 'src/app/app-constants';
import { Loger } from 'src/app/utils/decorator-helper';
import { AuthUserService } from 'src/app/repository/auth-user.service';
import { UserStateService } from 'src/app/repository/user-state.service';
import { IAuthFormValue } from 'src/interfaces/components-data';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ttip-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnDestroy, OnInit {
  formData = AUTH_DATA;
  buttonParameter = SEND_BTN_DATA;
  loginSubscription!: Subscription;
  errorMsg?: string;
  constructor(private authService: AuthUserService, private userState: UserStateService, private router: Router) {}

  ngOnInit() {}
  @Loger
  autorisatie(data: IAuthFormValue): void {
    this.loginSubscription = this.authService
      .login(data)
      .pipe(tap((user) => this.userState.setUserInfo(user)))
      .subscribe(
        () => void this.router.navigateByUrl('tracker'),
        (err) => {
          console.error(err);
          this.errorMsg = `Ошибка авторизации  `;
        }
      );
  }

  ngOnDestroy() {
    this.loginSubscription.unsubscribe();
  }
}
