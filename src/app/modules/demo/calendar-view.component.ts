import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

import { MockService } from '../../repository/mock.service';

import { IDateTabData, IActionButtonData } from '../../../interfaces/components-data';

import { DAY_BTN_DATA, WEEK_BTN_DATA } from '../../app-constants';

@Component({
  selector: 'ttip-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss'],
})
export class CalendarViewComponent implements OnInit, OnDestroy {
  actionButtonData!: IActionButtonData;

  actionButtonDayData = DAY_BTN_DATA;
  actionButtonWeekData = WEEK_BTN_DATA;

  serverTabData!: IDateTabData[];
  dateTabData: IDateTabData[] = [];
  dateTabSubscription!: Subscription;

  viewType = 'day';

  selectedIndex?: number;

  constructor(private mockService: MockService) {}

  ngOnInit(): void {
    this.initDateTabData();
  }

  getImputValue(e: string): void {
    console.log(e, '<<<');
  }

  dateTabSelected(action: number): void {
    this.selectedIndex = action;
  }

  setTabView(type: 'day' | 'week'): void {
    this.viewType = type;
    console.log(this.viewType, typeof type);

    if (type === 'week') {
      this.selectedIndex = 0;
      this.dateTabData = this.dateTabData.filter((_, index) => index % 7 === 0);
    } else {
      this.dateTabData = this.serverTabData;
    }
  }

  /**
   * @description
   * Create a mock data for action button
   */
  private initDateTabData(): void {
    this.dateTabSubscription = this.mockService
      .getDataForDateTabs()
      .pipe(
        tap((data) => (this.serverTabData = data)),
        tap((data) => (this.dateTabData = data.slice()))
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.dateTabSubscription.unsubscribe();
  }
}
