import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CalendarViewComponent } from './calendar-view.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MockService } from '../../repository/mock.service';
import { UiModule } from 'src/app/components/ui.module';
import { FormViewComponent } from './form-view/form-view.component';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [CalendarViewComponent, FormViewComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: CalendarViewComponent }]),
    UiModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  exports: [RouterModule],
  providers: [MockService],
})
export class DemoModule {}
