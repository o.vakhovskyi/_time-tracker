import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import {
  PATTERN_FOR_TIME,
  OPTIONS_FOR_SELECT_PROJECTS,
  TEXT_AREA_DATA,
  SEND_BTN_DATA,
  INPUT_TYPE_NUMBER_DATA,
} from 'src/app/app-constants';
@Component({
  selector: 'ttip-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.scss'],
})
export class FormViewComponent implements OnInit {
  form!: FormGroup;

  sendButton = SEND_BTN_DATA;
  inputTimeData = INPUT_TYPE_NUMBER_DATA;
  textAreaData = TEXT_AREA_DATA;
  optionsForSelect = OPTIONS_FOR_SELECT_PROJECTS;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.initializeForm();
  }
  // FormGroup
  initializeForm(): void {
    this.form = this.formBuilder.group({
      textarea: new FormControl('some', [Validators.required]),
      inputTime: new FormControl(this.inputTimeData.value, [Validators.pattern(PATTERN_FOR_TIME), Validators.required]),
      select: new FormControl('', [Validators.required]),
    });
  }

  sendFormData(): void {
    console.log(this.form.value);
  }
}
