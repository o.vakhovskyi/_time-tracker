import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { IActionButtonData, ICacheData, IFormValue, IOptionData } from 'src/interfaces/components-data';
import { PATTERN_FOR_TIME } from 'src/app/app-constants';

@Component({
  selector: 'ttip-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.scss'],
})
export class FormViewComponent implements OnInit {
  form!: FormGroup;
  patternForTime = PATTERN_FOR_TIME;

  @Input() sendButton!: IActionButtonData;
  @Input() formData!: ICacheData;

  @Output() sendFormValue: EventEmitter<IFormValue> = new EventEmitter<IFormValue>();

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm(): void {
    this.form = this.formBuilder.group({
      textarea: new FormControl(this.formData.job_notes.value, [Validators.required]),
      inputTime: new FormControl(this.formData.input_time.value, [
        Validators.pattern(this.patternForTime),
        Validators.required,
      ]),
      selectProject: new FormControl(this.findSelectedOptions(this.formData.selected_project), [Validators.required]),
      selectTask: new FormControl(this.findSelectedOptions(this.formData.selected_task), [Validators.required]),
    });
  }

  sendFormData(): void {
    if (this.form.status === 'VALID') {
      this.sendFormValue.emit(this.form.value);
      this.form.reset();
      this.form.markAsUntouched();
    }
  }
  private findSelectedOptions(arr: IOptionData[]): string {
    let selectedOption = '';
    arr.map((i) => {
      if (i.selected === true) {
        selectedOption = i.value;
      }
    });

    return selectedOption;
  }
}
