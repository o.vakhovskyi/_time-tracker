import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ttip-time-card',
  templateUrl: './time-card.component.html',
  styleUrls: ['./time-card.component.scss'],
})
export class TimeCardComponent implements OnInit {
  @Input() daysTime!: number;
  constructor() {}

  ngOnInit(): void {}
}
