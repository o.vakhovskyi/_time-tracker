import { Component, OnInit, OnDestroy } from '@angular/core';
import { defer, of, Subscription } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';

import { MockService } from '../../../../repository/mock.service';

import { IDateTabData, IActionButtonData, ICacheData, IFormValue } from '../../../../../interfaces/components-data';

import { SEND_BTN_DATA, CACHE_FORM_DATA } from 'src/app/app-constants';
import { Loger } from 'src/app/utils/decorator-helper';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ttip-time-tracking',
  templateUrl: './time-tracking.component.html',
  styleUrls: ['./time-tracking.component.scss'],
})
export class PagesComponent implements OnInit, OnDestroy {
  actionButtonData!: IActionButtonData;
  serverTabData!: IDateTabData[];
  dateTabData: IDateTabData[] = [];
  formData!: ICacheData;

  dateTabSubscription!: Subscription;
  getCacheDataSubscription!: Subscription;
  activatedRouteSubscription!: Subscription;

  viewType = 'day';
  daysTime = 0;
  selectedIndex = 0;
  sendButtonData = SEND_BTN_DATA;

  constructor(private mockService: MockService, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.initDateTabData();
    this.activatedRouteSubscription = this.activatedRoute.queryParams
      .pipe(switchMap((v) => defer(() => (v.cache === 'yes' ? of(this.setFormDataFromCache()) : this.getFormData()))))
      .subscribe();
  }

  private getFormData(): void {
    this.getCacheDataSubscription = this.mockService
      .getMockForTrackerPage()
      .pipe(
        catchError((_err) => {
          console.error(_err);

          return of(this.setFormDataFromCache());
        })
      )

      .subscribe((data) => (this.formData = data));
  }

  private setFormDataFromCache(): ICacheData {
    return (this.formData = CACHE_FORM_DATA);
  }

  private initDateTabData(): void {
    this.dateTabSubscription = this.mockService
      .getDataForDateTabs()
      .pipe(
        tap((data) => (this.serverTabData = data)),
        tap((data) => (this.dateTabData = data.slice())),
        tap((_) => this.dateTabSelected(new Date().getDate() - 1))
      )
      .subscribe();
  }
  @Loger
  dateTabSelected(action: number): void {
    this.selectedIndex = action;
  }

  setTabView(type: 'day' | 'week'): void {
    this.viewType = type;
    if (type === 'week') {
      this.selectedIndex = 0;
      this.dateTabData = this.dateTabData.filter((_, index) => index % 7 === 0);
    } else {
      this.dateTabData = this.serverTabData;
    }
  }

  @Loger
  tickFormData(data: IFormValue): void {
    this.daysTime = data.inputTime;
    console.log(this.dateTabData[this.selectedIndex]);
    this.mockService.sendFormData(JSON.stringify(data));
  }

  ngOnDestroy(): void {
    this.activatedRouteSubscription.unsubscribe();
    this.dateTabSubscription.unsubscribe();
    this.getCacheDataSubscription.unsubscribe();
  }
}
