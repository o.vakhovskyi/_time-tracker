import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PagesComponent } from './pages/time-tracking-page/time-tracking.component';
import { FormViewComponent } from './components/form-view/form-view.component';

import { MockService } from '../../repository/mock.service';

import { UiModule } from 'src/app/components/ui.module';

import { MatCardModule } from '@angular/material/card';
import { AuthGuards } from 'src/app/auth-guards';
@NgModule({
  declarations: [PagesComponent, FormViewComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: PagesComponent }]),
    UiModule,
    MatCardModule,
  ],
  exports: [RouterModule],
  providers: [MockService],
})
export class TimeTreckingModule {}
