import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { IUserProfile, IAuthFormValue } from 'src/interfaces/components-data';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { UserStateService } from 'src/app/repository/user-state.service';

@Injectable()
export class AuthUserService {
  constructor(private http: HttpClient, private router: Router, private userState: UserStateService) {}

  login(data: IAuthFormValue): Observable<IUserProfile> {
    const link = 'app/repository/users-data-mock.json';

    if (data.login === 'user') {
      return this.http.get<IUserProfile>(link).pipe(catchError((err) => throwError(err)));
    } else {
      return throwError('Пользователь не найден :(');
    }
  }

  logOut() {
    this.userState.removeToken();
  }
}
