import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';

import { IDateTabData, ICacheData } from '../../interfaces/components-data';

import createDataArray from '../utils/creater-date-array';

import { HttpClient } from '@angular/common/http';

@Injectable()
export class MockService {
  constructor(private http: HttpClient) {}
  getDataForDateTabs(): Observable<IDateTabData[]> {
    return of(createDataArray());
  }

  getMockForTrackerPage(): Observable<ICacheData> {
    const link = 'app/repository/form-view-mock.json';
    if (this.randomNumber(1, 3) === 3) {
      return throwError(new Error('oops! Request error :('));
    }

    return this.http.get<ICacheData>(link);
  }

  sendFormData(data: string): void {
    console.log('Form data was send to server', data);
  }

  private randomNumber(min: number, max: number): number {
    const res: number = min + Math.random() * (max + 1 - min);

    return Math.round(res);
  }
}
