import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IUserProfile } from 'src/interfaces/components-data';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UserStateService {
  private user = new BehaviorSubject<IUserProfile | null>(null);
  userObs: Observable<IUserProfile | null> = this.user.asObservable();
  private hasToken: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private router: Router) {
    if (localStorage.getItem('token')) {
      this.setIsAuth = true;
    }
  }

  get isAuth(): Observable<boolean> {
    return this.hasToken.asObservable();
  }

  set setIsAuth(value: boolean) {
    this.hasToken.next(value);
  }

  setUserInfo(i: IUserProfile): void {
    this.user.next(i);
    this.setTokenToLocalStorage = i.token;
    this.setIsAuth = true;
  }

  set setTokenToLocalStorage(token: string) {
    localStorage.setItem('token', token);
  }

  removeToken() {
    localStorage.removeItem('token');
    this.setIsAuth = false;
    void this.router.navigateByUrl('/');
  }

  isAuthenticated(): Observable<boolean> {
    return this.hasToken.asObservable();
  }
}
