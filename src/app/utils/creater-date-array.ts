import * as dayjs from 'dayjs';
import * as weekOfYear from 'dayjs/plugin/weekOfYear';
import * as weekday from 'dayjs/plugin/weekday';
dayjs.extend(weekOfYear);
dayjs.extend(weekday);

import { IDateTabData } from '../../interfaces/components-data';

export default function createDataArray(): IDateTabData[] {
  // const dataArr: Array<IDateTab.Data> = [];
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth();
  const monthNow = getFirstDay(date, month); // первый день текущего мес - 4
  const firstDayStamp = new Date(year, month, 1, 0, 0, 0, 0).getTime(); // stamp  первого дня текущег месяца
  const monthNext = getFirstDay(date, month + 1); // первый день след мес

  const hoursPerDay = 24;
  const secondsPerHour = 3600;
  const millisecondInSecond = 1000;
  const daysInMonth = Math.round((monthNext - monthNow) / millisecondInSecond / secondsPerHour / hoursPerDay); // количество дней
  const dataArr = createData(daysInMonth, firstDayStamp);

  return dataArr;
}

function getFirstDay(date: Date, month: number): number {
  return date.setMonth(month, 1);
}

function createData(daysInMonth: number, firstDayStamp: number): IDateTabData[] {
  const dataArr: IDateTabData[] = [];
  const millisecondPerDay = 86400000;
  for (let i = 0; i < daysInMonth; i++) {
    const s = firstDayStamp + i * millisecondPerDay;
    dataArr.push({
      selected: true,
      isActive: true,
      value: s,

      disabled: false,
    });
  }

  return dataArr;
}
export function getCurrentDayNumber() {
  return dayjs().date();
}
