export function Loger(
  target: any,
  propertyKey: string,
  propertyDescriptor: TypedPropertyDescriptor<(...data: any) => void>
): any {
  const dec = propertyDescriptor.value as () => void;
  propertyDescriptor.value = function (this, ...arg: []) {
    console.group('Method name: ', propertyKey);

    arg.map((i: []) => {
      if (typeof i === 'object') {
        for (const key in i) {
          if (key) {
            // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            console.log(`${key}:  ${i[key]}`);
          }
        }
      }
      if (typeof i === 'number' || 'string') {
        console.log(`${typeof i}: ${i}`);
      }
    });
    // eslint-disable-next-line no-restricted-syntax
    console.groupEnd();
    const result = dec.apply(this, arg);

    return result;
  };
}
