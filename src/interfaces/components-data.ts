import { ThemePalette } from '@angular/material/core';

export interface IActionButtonData {
  color: ThemePalette;
  disabled: boolean;
  placeholder: string;
  autoFocus: boolean;
  icon?: string;
  isRoundButton?: boolean;
  type?: string;
}

export interface IDateTabData {
  selected: boolean;
  isActive: boolean;
  disabled: boolean;
  value: number;
}
export interface IInputData {
  type: string;
  value?: number | string;
  placeholder: string;
  required?: boolean;
  pattern?: string;
  label?: string;
  visibilityValue?: boolean;
  icon?: string;
}
export interface IOptionData {
  value: string;
  title: string;
  selected?: boolean;
}
export interface ICacheData {
  selected_project: IOptionData[];
  selected_task: IOptionData[];
  job_notes: IInputData;
  input_time: IInputData;
}
export interface IAuthData {
  loginData: IInputData;
  passwordData: IInputData;
}

export interface IFormValue {
  inputTime: number;
  textarea: string;
  selectProject: string;
  selectTask: string;
}
export interface IAuthFormValue {
  login: string;
  password: string;
}

export interface IUserProfile {
  name: string;
  familyName: string;
  login: string;
  email: string;
  password: string;
  token: string;
}
